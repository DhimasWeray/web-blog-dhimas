class CommentsController < ApplicationController
  before_action :authenticate_user!, except: %i[index show]
  def create
    @article = Article.find(params[:article_id])
    @comment = @article.comments.create(comment_params)
    redirect_to article_path(@article)
  end

  def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    @comment.destroy
    redirect_to article_path(@article), status: :see_other
  end

  def edit
    @comment = Comment.find(params[:id])
  end

  def update
    # binding.pry
    @comment = Comment.find(params[:id])
    if @comment.update(update_comment_params)
      redirect_to article_path(@comment)
    else
      @comment.errors
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:commenter, :body, :status, :user_id)
  end

  def update_comment_params
    params.permit(:commenter, :body, :status, :user_id)
  end
end
