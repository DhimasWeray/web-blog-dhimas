Before { @user = User.find(1) }

When("I fill my credential") do
  expect(find("form.new_user"))
  fill_in "Email", with: "test2@gmail.com"
  fill_in "Password", with: "123456"
end

When("I click Log in") { click_on "Log in" }

Then("I should see {string}") { |string| expect(find("div.alert")) }

Given("I am on the log in page") { visit new_user_session_path }

Given("I am on the home page") { visit root_path }

Then("I should see all post") { Article.all }

Given("I am on create post page") { visit new_article_path }

When /^I fill "(.*?)" with "(.*?)"$/ do |field, value|
  fill_in field, with: value
end

Then ("I get an error message") do
  @post = Article.new(@params)
end

Then("I was created Post") do
  @post = Article.new(@params)
  @response = @post.save
end

When("I click show post") { find(".card", match: :first).click_on("show") }

Then("I should success show detail post") { visit articles_url }

Given("I am on the post page") { visit articles_path }

When("I click destroy post with {string} = {string}") do |post_id, id|
  @post = Article.find_by_id(id)
end

Then("I should success delete my post") { @post.destroy }

When("I click edit post with {string} = {string}") do |post_id, id|
  @params = { title: "", body: "", user_id: @user.id }
  @post = Article.find_by_id(id)
end

When("I edit {string} with {string}") do |attribute, value|
  @params[attribute] = value
end

Then("I should success edit my post") { @post = Article.update(@params) }
