Rails.application.routes.draw do
  devise_for :users
  mount LetterOpenerWeb::Engine, at: '/letter_opener' if Rails.env.development?
  root "articles#index"
  get '/comments/:id/edit', to: 'comments#edit', as: :edit_comment
  put '/comments/:id/edit', to: 'comments#update', as: :update_comment

  resources :articles do
    resources :comments
  end
end
