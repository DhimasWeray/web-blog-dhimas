require 'rails_helper'

RSpec.describe Article, type: :model do
  user_id = User.ids.first
  article = Article.new(title: "test", body: "testing", status: "public", user_id: user_id ) 
  it "is valid with valid attributes" do
    expect(article).to be_valid
  end

  it "is not valid without a title" do
    article.title = nil
    expect(article).to_not be_valid
  end

  it "is not valid without a body" do
    article.body = nil
    expect(article).to_not be_valid
  end

  it "is not valid because user not exist" do
    article.user_id = 100
    expect(article).to_not be_valid
  end
end
